![Build Status](https://img.shields.io/bitbucket/pipelines/silte/movescount-summary.svg)

# Movescount Redirect React App

Do you have a struggle to get your logs from a specific timespan? Do you absolutely hate to get it to work? Say no more, this app will help you achieve just that and makes your life easier to track.

## App Function

You are able to select your name (which has an ID associated with it) from the `Name` list. Choosing starting and ending dates determine which timespan logs you'll see. Clicking on the `Calendar` icon will give you the last month automaticly (from current date 30 days back or 4 weeks). You can change format from date to week by clicking `Week` or `Date`, either one is visible only at once.

## Available Scripts

In the project directory, you can run:

#### `npm install`

Installs all needed dependencies.

#### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### No comments? Why?

`Clean code is self-commenting`. I think this App does just that.
https://americanexpress.io/clean-code-dirty-code/