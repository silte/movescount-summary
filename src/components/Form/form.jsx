import React from 'react';

import { Option, DateForm, WeekForm } from './';
import { Home } from '../App';

const API = 'https://treenit.silte.fi/data.json';

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;

    this.state = {
      data: [],
      showDateForm: false,
    };
  }

  componentDidMount() {
    fetch(API)
      .then(response => response.json())
      .then(data => this.setState({ data }))
      .catch(() => this.setState({ data: [{ name: 'Oma', id: '' }] }));
  }

  getMonday = (w, y) => {
    const simple = new Date(Date.UTC(y, 0, 1 + ((w - 1) * 7)));
    const dow = simple.getDay();
    const ISOweekStart = simple;

    if (dow <= 4) {
      ISOweekStart.setDate(((simple.getDate() - simple.getDay()) + 1));
    } else {
      ISOweekStart.setDate((simple.getDate() + 8) - simple.getDay());
    }

    return ISOweekStart.toISOString().split('T')[0];
  };

  getSunday(w, y) {
    const sunday = new Date(Date.UTC(y, 0, ((1 + (w - 1)) * 7)));

    while (sunday.getDay() !== 0) {
      sunday.setDate(sunday.getDate() - 1);
    }
    return sunday.toISOString().split('T')[0];
  }

  formatDate = (date) => {
    const year = date.getFullYear();

    const month = () => {
      const actualMonth = date.getMonth();

      return actualMonth < 9 ? `0${actualMonth + 1}` : actualMonth + 1;
    };

    const day = () => {
      const actualDay = date.getDate();

      return actualDay < 9 ? `0${actualDay}` : actualDay;
    };

    return `${year}-${month()}-${day()}`;
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const {
      end, start, select, startWeek, endWeek,
    } = e.target;
    const selectValue = select.options[select.selectedIndex].value;
    const { showDateForm } = this.state;

    if (!showDateForm) {
      const startWeekYear = startWeek.value.slice(0, 4);
      const startWeekWeek = startWeek.value.slice(-2);
      const endWeekYear = endWeek.value.slice(0, 4);
      const endWeekWeek = endWeek.value.slice(-2);

      let weekStart = this.getMonday(startWeekWeek, startWeekYear);
      let weekEnd = this.getSunday(endWeekWeek, endWeekYear);

      const generatedLinkWeek = `https://www.movescount.com/summary?memberId=${selectValue}#moves-from=${weekStart}&moves-to=${weekEnd}`;
      const generatedLinkNoIdWeek = `https://www.movescount.com/summary#moves-from=${weekStart}&moves-to=${weekEnd}`;

      if (!weekStart && !weekEnd && !selectValue) {
        const element = document.createElement('div');
        element.classList.add('container', 'container--inset', 'message');
        const referenceNode = document.querySelector('.movescount-modal');

        if (referenceNode.nextSibling === null) {
          referenceNode.parentNode.insertBefore(element, referenceNode.nextSibling);
        }

        const heading = document.createElement('h2');
        heading.classList.add('message__title');
        const headingText = document.createTextNode('Insufficient Data');
        heading.appendChild(headingText);
        element.appendChild(heading);
      } else if (!selectValue && weekStart === '1899-12-25' && weekEnd === '1899-12-31') {
        window.location.href = `${generatedLinkNoIdWeek}`;
      } else {
        window.location.href = `${generatedLinkWeek}`;
      }

      weekStart = '';
      weekEnd = '';
    }

    if (showDateForm) {
      const generatedLink = `https://www.movescount.com/summary?memberId=${selectValue}#moves-from=${start.value}&moves-to=${end.value}`;
      const generatedLinkNoId = `https://www.movescount.com/summary#moves-from=${start.value}&moves-to=${end.value}`;

      if (!start.value && !end.value && !selectValue) {
        const element = document.createElement('div');
        element.classList.add('container', 'container--inset', 'message');
        const referenceNode = document.querySelector('.movescount-modal');

        if (referenceNode.nextSibling === null) {
          referenceNode.parentNode.insertBefore(element, referenceNode.nextSibling);
        }

        const heading = document.createElement('h2');
        heading.classList.add('message__title');
        const headingText = document.createTextNode('Insufficient Data');
        heading.appendChild(headingText);
        element.appendChild(heading);
      } else if (!selectValue && end.value && start.value) {
        window.location.href = `${generatedLinkNoId}`;
      } else {
        window.location.href = `${generatedLink}`;
      }

      end.value = '';
      start.value = '';
    }
    select.value = '';
  }

  handle30Days = (e) => {
    e.preventDefault();
    const start = document.querySelector('#start');
    const end = document.querySelector('#end');
    const startWeek = document.querySelector('#startWeek');
    const endWeek = document.querySelector('#endWeek');
    const currentDate = this.formatDate(new Date());
    const priorDate = new Date(new Date().setDate(new Date().getDate() - 30)).toISOString();
    const priorDate28 = new Date(new Date().setDate(new Date().getDate() - 28)).toISOString();
    const priorDateValue = priorDate.split('T')[0];
    const priorDateValue28 = priorDate28.split('T')[0];
    const currentYear = new Date().getFullYear();

    const priorDateValueYear = priorDateValue28.slice(0, 4);
    const priorDateValueMonth = priorDateValue28.slice(5, 7);
    const priorDateValueDay = priorDateValue28.slice(8, 10);
    const priorDateValueYearInt = parseInt(priorDateValueYear, 10);
    const priorDateValueMonthInt = parseInt(priorDateValueMonth, 10);
    const priorDateValueDayInt = parseInt(priorDateValueDay, 10);

    function DateToWeek(dt) {
      const tdt = new Date(dt.valueOf());
      const dayn = (dt.getDay() + 6) % 7;
      tdt.setDate((tdt.getDate() - dayn) + 3);
      const firstThursday = tdt.valueOf();
      tdt.setMonth(0, 1);

      if (tdt.getDay() !== 4) {
        tdt.setMonth(0, 1 + (((4 - tdt.getDay()) + 7) % 7));
      }

      return 1 + Math.ceil((firstThursday - tdt) / 604800000);
    }

    const ThirtyDaysBack = `${priorDateValueYearInt}, ${priorDateValueMonthInt}, ${priorDateValueDayInt}`;
    const ThirtyDaysBackValue = DateToWeek(new Date(ThirtyDaysBack));

    const itsDate = Date;
    // eslint-disable-next-line
    itsDate.prototype.getWeekNumber = function () {
      const d = new Date(Date.UTC(this.getFullYear(), this.getMonth(), this.getDate()));
      const dayNum = d.getUTCDay() || 7;
      d.setUTCDate((d.getUTCDate() + 4) - dayNum);
      const yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
      return Math.ceil((((d - yearStart) / 86400000) + 1) / 7);
    };

    const currentWeek = `${currentYear}-W${new Date().getWeekNumber()}`;
    const fourWeeksBack = `${priorDateValueYearInt}-W${ThirtyDaysBackValue}`;
    const { showDateForm } = this.state;

    if (showDateForm) {
      start.value = priorDateValue;
      end.value = currentDate;
    } else {
      startWeek.value = fourWeeksBack;
      endWeek.value = currentWeek;
    }
  }

  handleChangeForm = (e) => {
    e.preventDefault();

    const button = document.querySelector('#formType');
    const { showDateForm } = this.state;

    button.textContent = showDateForm ? button.textContent = 'Date' : button.textContent = 'Week';

    this.setState(prevState => ({ showDateForm: !prevState.showDateForm }));
  }

  render() {
    const { data, showDateForm } = this.state;

    return (
      <div className="movescount-modal utils--shadow">
        <Home />
        <form onSubmit={this.handleSubmit} className="movescount-form">
          <div className="form-item">
            <label htmlFor="select">Name</label>
            <select name="select" id="select" className="movescount-form__select">
              <Option data={data} />
            </select>
          </div>
          { showDateForm ? <DateForm formatDate={this.formatDate} /> : <WeekForm /> }
          <div className="btn-container btn-container--3 utils--mrb-0">
            <button onClick={this.handleChangeForm} id="formType" className="btn btn--gray btn--big-text">Date</button>
            <button onClick={this.handle30Days} className="btn btn--gray btn--big-text"><i className="far fa-calendar-alt" /></button>
            <button type="submit" className="btn btn--green btn--big-text">
              <i className="fas fa-arrow-right" />
            </button>
          </div>
        </form>
      </div>
    );
  }
}

export { Form };

