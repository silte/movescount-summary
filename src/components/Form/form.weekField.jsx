import React from 'react';
import PropTypes from 'prop-types';

const WeekField = ({
  name, id, label, className,
}) => (
  <div className="week-form__item">
    <label htmlFor={name}>{label}</label>
    <input type="week" name={name} id={id} className={className} />
  </div>
);

WeekField.defaultProps = {
  label: false,
};

WeekField.propTypes = {
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  className: PropTypes.string.isRequired,
};

export { WeekField };
