import React from 'react';
import PropTypes from 'prop-types';

import { DateField } from './';

const DateForm = ({ formatDate }) => (
  <div className="date-form form-item">
    <DateField formatDate={formatDate} name="start-date" id="start" label="Start" className="movescount-form__input" />
    <DateField formatDate={formatDate} name="end-date" id="end" label="End" className="movescount-form__input" />
  </div>
);

DateField.defaultProps = {
  formatDate: true,
};

DateForm.propTypes = {
  formatDate: PropTypes.func.isRequired,
};

export { DateForm };
