import React from 'react';
import PropTypes from 'prop-types';


const Option = ({ data }) => (
  data.map(i => <option value={i.id} key={i.id}>{i.name}</option>)
);

Option.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string,
  })),
};

export { Option };
