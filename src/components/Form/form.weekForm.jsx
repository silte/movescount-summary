import React from 'react';

import { WeekField } from './';

const WeekForm = () => (
  <div className="week-form form-item">
    <WeekField name="startWeek" id="startWeek" label="Start" className="movescount-form__input" />
    <WeekField name="endWeek" id="endWeek" label="End" className="movescount-form__input" />
  </div>
);

export { WeekForm };
