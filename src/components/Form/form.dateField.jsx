import React from 'react';
import PropTypes from 'prop-types';

const DateField = ({
  formatDate, name, id, label, className,
}) => (
  <div className="date-form__item">
    <label htmlFor={name}>{label}</label>
    <input
      type="date"
      id={id}
      key={id}
      max={formatDate(new Date())}
      name={name}
      className={className}
    />
  </div>
);

DateField.defaultProps = {
  formatDate: false,
  label: false,
};

DateField.propTypes = {
  formatDate: PropTypes.func,
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  className: PropTypes.string.isRequired,
};

export { DateField };
