import React from 'react';

import { Form } from '../Form';

const App = () => (
  <div className="container container--inset center-content">
    <Form />
  </div>
);

export { App };
