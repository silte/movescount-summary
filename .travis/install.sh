#!/bin/bash
openssl aes-256-cbc -K $encrypted_8987408eb4e2_key -iv $encrypted_8987408eb4e2_iv -in .travis/deploy_rsa.enc -out .travis/deploy_rsa -d
chmod 600 .travis/deploy_rsa
mv .travis/deploy_rsa ~/.ssh/id_rsa